Tunnel Vision Web Browser Extension
A web browser extension that detects when a web address returns 
different results for different users. This will allow web users to 
detect that they are being 'Tunnel Visioned'.

Here are two naive implementation ideas:

(a)
1. The extension sends a GET request to the URL currently being 
   visited by the user.
2. The extension diffs the HTML it got from URL against the HTML 
   being displayed to the user.
   
   Problems:
	1. This will not work properly if the 'tunnel visioning' is being
	   performed at the IP address level and not the web-session level
	   because the web-extension and browser are using the same 
	   IP address.
(b)
1. Have the extension send the URL the user is visiting to multiple 
   remote servers located in different geographical locations. 
2. Remote servers receive the URL and send out their own GET requests 
   to the URL, 
3. Remote servers hashes their HTML results
4. Remote server sends the hashed result back to the user.
5. Extension waits until it receives all results from the remote 
   servers
6. Extension compares returned hashes from the remote servers.

	Problems:
	  1. Could be expensive sending requests to multiple remote 
	     servers
	  2. Could leak URL


	
